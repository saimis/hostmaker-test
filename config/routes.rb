Rails.application.routes.draw do
  resources :owndoms
  resources :countries
  resources :cities
  resources :owners
  mount JasmineRails::Engine => '/specs' if defined?(JasmineRails)

  root to: 'administration#index'

  namespace :api, constraints: { format: 'json' }  do
    namespace :v1 do
      get 'owndoms/:id' => 'owndoms#show'
      get 'owndoms'     => 'owndoms#index'
    end
  end
end
