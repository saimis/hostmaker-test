class CreateOwndoms < ActiveRecord::Migration[5.1]
  def change
    create_table :owndoms do |t|
      t.belongs_to :owner, index: true
      t.references :country, index: true
      t.references :city, index: true

      t.string :address1
      t.string :address2
      t.string :address3
      t.string :address4
      t.string :post_code
      t.decimal :income, :precision => 10, :scale => 2

      t.timestamps
    end
  end
end
