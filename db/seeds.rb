# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
OWNDOMS_LIST = [
      {
        "owner": "carlos",
        "address": {
            "line1": "Flat 5",
            "line4": "7 Westbourne Terrace",
            "postCode": "W2 3UL",
            "city": "London",
            "country": "U.K."
        },
        "incomeGenerated": 2000.34
      },
      {
        "owner": "ankur",
        "address": {
            "line1": "4",
            "line2": "Tower Mansions",
            "line4": "86-87 Grange Road",
            "postCode": "SE1 3BW",
            "city": "London",
            "country": "U.K."
        },
        "incomeGenerated": 1000
      },
      {
        "owner": "elaine",
        "address": {
            "line1": "4",
            "line2": "332b",
            "line4": "Goswell Road",
            "postCode": "EC1V 7LQ",
            "city": "London",
            "country": "U.K."
        },
        "incomeGenerated": 1200
      },
      {
        "owner": "irene",
        "address": {
            "line1": "4",
            "line2": "3",
            "line4": "Downfield Rd",
            "postCode": "EC1V 7LQ",
            "city": "London",
            "country": "U.K."
        },
        "incomeGenerated": 1200
      },

      {
        "owner": "lin",
        "address": {
            "line1": "",
            "line2": "4A",
            "line4": "Sisters Ave",
            "postCode": "EC1V 7LQ",
            "city": "London",
            "country": "U.K."
        },
        "incomeGenerated": 1200
      },

      {
        "owner": "max",
        "address": {
            "line1": "Apartment No. 40",
            "line2": "3",
            "line4": "Pinner Rd",
            "postCode": "EC1V 7LQ",
            "city": "London",
            "country": "U.K."
        },
        "incomeGenerated": 1200
      }
    ]

    OWNDOMS_LIST.each do |owndom|
      city = City.find_or_create_by(name: owndom[:address][:city])
      country = Country.find_or_create_by(name: owndom[:address][:country])
      owner = Owner.find_or_create_by(name: owndom[:owner])

      Owndom.create(
        owner_id: owner.id,
        country_id: country.id,
        address1: owndom[:address][:line1] || '',
        address2: owndom[:address][:line2] || '',
        address3: owndom[:address][:line3] || '',
        address4: owndom[:address][:line4] || '',
        post_code: owndom[:address][:post_code] || '',
        city_id: city.id,
        income: owndom[:incomeGenerated] || 0,
        created_at: Time.zone.parse('2017-07-27 00:00:00'),
        updated_at: Time.zone.parse('2017-07-27 00:00:00')
      )
    end
