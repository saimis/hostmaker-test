json.array! @owndoms do |owndom|
  json.partial!('api/v1/owndoms/owndom', owndom: owndom)
end
