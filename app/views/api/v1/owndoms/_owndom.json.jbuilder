json.id owndom.id
json.owner owndom.owner.name
json.address do
  json.address1 owndom.address1
  json.address2 owndom.address2
  json.address3 owndom.address3
  json.address4 owndom.address4
  json.city owndom.city.name
  json.postCode owndom.post_code
  json.country owndom.country.name
end
json.income owndom.income
