json.extract! owndom, :id, :address1, :address2, :address3, :address4, :post_code, :income, :created_at, :updated_at
json.url owndom_url(owndom, format: :json)
