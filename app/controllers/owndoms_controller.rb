class OwndomsController < ApplicationController
  before_action :set_owndom, only: [:show, :edit, :update, :destroy]

  # GET /owndoms
  # GET /owndoms.json
  def index
    @owndoms = Owndom.all
  end

  # GET /owndoms/1
  # GET /owndoms/1.json
  def show
  end

  # GET /owndoms/new
  def new
    @owndom = Owndom.new
  end

  # GET /owndoms/1/edit
  def edit
  end

  # POST /owndoms
  # POST /owndoms.json
  def create
    @owndom = Owndom.new(owndom_params)

    respond_to do |format|
      if @owndom.save
        format.html { redirect_to @owndom, notice: 'Owndom was successfully created.' }
        format.json { render :show, status: :created, location: @owndom }
      else
        format.html { render :new }
        format.json { render json: @owndom.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /owndoms/1
  # PATCH/PUT /owndoms/1.json
  def update
    respond_to do |format|
      if @owndom.update(owndom_params)
        format.html { redirect_to @owndom, notice: 'Owndom was successfully updated.' }
        format.json { render :show, status: :ok, location: @owndom }
      else
        format.html { render :edit }
        format.json { render json: @owndom.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /owndoms/1
  # DELETE /owndoms/1.json
  def destroy
    @owndom.destroy
    respond_to do |format|
      format.html { redirect_to owndoms_url, notice: 'Owndom was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_owndom
      @owndom = Owndom.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def owndom_params
      params.require(:owndom).permit(:address1, :address2, :address3, :address4, :post_code, :income)
    end
end
