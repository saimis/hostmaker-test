class Api::BaseController < ActionController::Base
  before_action :allow_only_json

  rescue_from ActiveRecord::RecordInvalid, with: :unprocessable_entity
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found

  def unprocessable_entity(exception)
    render json: exception.record.errors, status: :unprocessable_entity
  end

  def record_not_found(exception)
    render json: { error: exception.message }, status: :not_found
  end

  private

  def allow_only_json
    render json: { error: 'Format not allowed' }, status: 406 unless request.format == :json
  end
end
