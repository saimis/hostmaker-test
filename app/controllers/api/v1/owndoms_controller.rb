class Api::V1::OwndomsController < Api::BaseController

  def index
    @owndoms = Api::V1::Owndoms.new(params).list
  end

  def show
    @owndom = Api::V1::Owndoms.new(params).show
  end
end
