import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import axios from 'axios'
import Map from './map'
import List from './list'

import './style.sass'

class OwndomsList extends Component {
  constructor(props) {
    super(props)

    this.state = {
      activeMarker: null,
      activeRow: null,
      owndoms: [],
      markersFound: false
    }

    this.onMarkersFound = this.onMarkersFound.bind(this)
    this.onMarkerHover = this.onMarkerHover.bind(this)
    this.onSearch = this.onSearch.bind(this)
    this.toggleMarker = this.toggleMarker.bind(this)
  }

  componentDidMount() {
    this.requestData()
  }

  toggleMarker(activeMarker) {
    this.setState({ activeMarker })
  }

  onMarkersFound() {
    this.setState({ markersFound: true })
  }

  onMarkerHover(activeRow) {
    this.setState({ activeRow })
  }

  onSearch(query) {
    this.requestData(query)
  }

  requestData(query) {
    axios.get(`/api/v1/owndoms.json`, { params: { query }}).then(res => {
      if (res.data) {
        this.setState({ owndoms: res.data })
      }
    })
  }

  render() {
    return(
      <div className="list-container__owndoms">
        <div className="owndoms__table-container">
          <List
            activeRow={this.state.activeRow}
            onSearch={this.onSearch}
            owndoms={this.state.owndoms}
            toggleMarker={this.toggleMarker}
          />
        </div>
        <div className="owndoms__map-container">
          <div className="map-container__owndoms-map" ref="map">
            <Map
              activeMarker={this.state.activeMarker}
              markersFound={this.state.markersFound}
              onMarkerHover={this.onMarkerHover}
              onMarkersFound={this.onMarkersFound}
              owndoms={this.state.owndoms}
            />
          </div>
        </div>
      </div>
    )
  }
}

ReactDOM.render(
  <OwndomsList />,
  document.getElementById('owndoms__list-container')
)
