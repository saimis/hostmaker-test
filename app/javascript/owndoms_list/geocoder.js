import async from 'async'
import axios from 'axios'

export function geocodeAddresses(owndoms, api_key, callback) {
  let google_api_url = `https://maps.googleapis.com/maps/api/geocode/json?key=${api_key}&address=`

  async.map(owndoms, (owndom, callback) => {
    // create single address line
    let address = Object.values(owndom.address).filter(Boolean).join(",")

    axios.get(`${google_api_url}${address}`).then((geo) => {
      let location = {}

      if (geo.data.status == 'OK') {
        location = {
          id: owndom.id,
          center: geo.data.results[0].geometry.location,
          title: geo.data.results[0].formatted_address
        }
      }

      callback(null, location)
    })
  }, (err, results) => {
    callback(results)
  })

}
