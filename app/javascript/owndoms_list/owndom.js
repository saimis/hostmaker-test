import React, { Component } from 'react'
import PropTypes from 'prop-types';

export default class Owndom extends Component {
  render() {
    return (
      <tr
        onMouseEnter={(e) => { this.props.toggleMarker(this.props.data.id) }}
        onMouseLeave={(e) => { this.props.toggleMarker(null) }}
        className={`owndoms-table__row ${this.props.active && 'row--active'}`}
      >
        <td>{this.props.data.owner}</td>
        <td className="row__owndom_column">
          {Object.values(this.props.data.address).map((address_line, index) => (
            <p key={[index, this.props.data.id].join('-') }>{address_line}</p>
          ))}
        </td>
        <td>{this.props.data.income}£</td>
      </tr>
    )
  }
}

Owndom.propTypes = {
  active: PropTypes.bool,
  data: PropTypes.object,
  toggleMarker: PropTypes.func,
};
