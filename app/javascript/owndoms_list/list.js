import React, { Component } from 'react'
import Owndom from './owndom'
import PropTypes from 'prop-types';

export default class List extends Component {
  constructor(props) {
    super(props)

    this.state = {
      query: ''
    }
  }

  onSearch(query) {
    this.setState({ query })
    this.props.onSearch(query)
  }

  render() {
    return (
      <table className="mdl-data-table mdl-js-data-table owndoms__owndoms-table">
        <thead>
          <tr>
            <th className="mdl-data-table__cell--non-numeric">Owner</th>
            <th>Address</th>
            <th>Income generated</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td colSpan="2">Search</td>
            <td><input type="text" name="query" value={this.state.query} onChange={(e) => this.onSearch(e.target.value)} /></td>
          </tr>
          {this.props.owndoms.map((owndom, index) => (
              <Owndom
                toggleMarker={this.props.toggleMarker}
                data={owndom} key={[owndom.id, index].join('-')}
                active={this.props.activeRow == owndom.id}
              />
            )
          )}
        </tbody>
      </table>
    )
  }
}

List.propTypes = {
  owndoms: PropTypes.array,
  activeRow: PropTypes.string,
  toggleMarker: PropTypes.func,
  onSearch: PropTypes.func
};
