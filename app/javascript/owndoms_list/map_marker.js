import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class MapMarker extends Component {
  render() {
    let active = this.props.activate || this.props.$hover

    return (
      <div className={`owndoms-map__map-marker ${active && 'map-marker--active'}`}>
        <div className={`map-marker__popup ${active && 'popup--active'}`}>{this.props.title}</div>
      </div>
    );
  }
}

MapMarker.propTypes = {
  activate: PropTypes.bool,
  $hover: PropTypes.bool,
  title: PropTypes.string,
};
