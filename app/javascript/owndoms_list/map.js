import React, { Component } from 'react'
import GoogleMapReact from 'google-map-react'
import MapMarker from './map_marker'
import { geocodeAddresses } from './geocoder.js'
import PropTypes from 'prop-types';

export default class Map extends Component {
  constructor(props) {
    super(props)

    this.state = {
      locations: [],
    }

    this.onChildMouseEnter = this.onChildMouseEnter.bind(this)
    this.onChildMouseLeave = this.onChildMouseLeave.bind(this)
    this.coordinatesFound = this.coordinatesFound.bind(this)
  }

  static defaultProps = {
    apiKey: 'AIzaSyC_lTVudLXE6wunT0hkn4kuSEpTqNuPUCY',
    center: { lat: 51.5073835, lng: -0.1277801 },
    circleBorderOpacity: 0.7,
    circleBorderWidth: 1,
    circleColor: '#57c687',
    circleOpacity: 0.2,
    serviceRadius: 20000,
    zoom: 10,
  }

  coordinatesFound(locations) {
    this.props.onMarkersFound()
    this.setState({ locations: locations })
  }

  componentDidUpdate() {
    if (!this.props.markersFound) {
      geocodeAddresses(this.props.owndoms, this.props.apiKey, this.coordinatesFound)
    }
  }

  drawServiceRadius = ({map, maps}) => {
    new maps.Circle({
      center: new google.maps.LatLng(this.props.center.lat, this.props.center.lng),
      fillColor: this.props.circleColor,
      fillOpacity: this.props.circleOpacity,
      map: map,
      radius: this.props.serviceRadius,
      strokeColor: this.props.circleColor,
      strokeOpacity: this.props.circleBorderOpacity,
      strokeWeight: this.props.circleBorderWidth,
    })
  }

  onChildMouseEnter(id) {
    this.props.onMarkerHover(id)
  }

  onChildMouseLeave() {
    this.props.onMarkerHover(null)
  }

  render() {
    const Owndoms = this.state.locations.map((location) => {
      // check if marker id exists in properties list (for ex. after search)
      if (this.props.owndoms.some((o) => ( o.id === location.id ))) {
        return (
          <MapMarker
            activate={this.props.activeMarker == location.id}
            key={location.id}
            lat={location.center.lat}
            lng={location.center.lng}
            title={location.title}
          />
        )
      }
    })

    return (
      <GoogleMapReact
        bootstrapURLKeys={{
          key: this.props.apiKey
        }}
        defaultCenter={this.props.center}
        defaultZoom={this.props.zoom}
        onChildMouseEnter={this.onChildMouseEnter}
        onChildMouseLeave={this.onChildMouseLeave}
        onGoogleApiLoaded={this.drawServiceRadius}
        yesIWantToUseGoogleMapApiInternals={true}
      >

      {Owndoms}
      </GoogleMapReact>
    )
  }
}

Map.propTypes = {
  owndoms: PropTypes.array,
  onMarkerHover: PropTypes.func,
  markersFound: PropTypes.bool,
  onMarkersFound: PropTypes.func,
  activeMarker: PropTypes.number
};
