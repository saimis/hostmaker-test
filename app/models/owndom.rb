class Owndom < ApplicationRecord
  belongs_to :owner
  belongs_to :country
  belongs_to :city

  accepts_nested_attributes_for :owner, :city, :country

  def as_json(options = {})
    super(include: [:owner, :country, :city])
  end

  def self.by_query(query)
    return where(nil) unless query.present?

    joins(:city).
    joins(:country).
    joins(:owner).
    where('LOWER(cities.name) LIKE :query OR LOWER(countries.name)  LIKE :query OR LOWER(owners.name) LIKE :query OR
      LOWER(address1) LIKE :query OR LOWER(address2) LIKE :query OR LOWER(address3) LIKE :query OR LOWER(address4) LIKE :query
      OR LOWER(post_code) LIKE :query', query: "%#{query.downcase}%")
  end
end
