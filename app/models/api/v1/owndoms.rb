module Api
  module V1
    class Owndoms
      attr_reader :params

      def initialize(params)
        @params  = params
      end

      def list
        Owndom.by_query(params[:query])
      end

      def show
        Owndom.find(params[:id])
      end
    end
  end
end
