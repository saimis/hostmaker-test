# Test app for Hostmaker


### Installation

App is tested on Ruby 2.4.0.

To install gems run
`$ bundle install`

Migrate database
`$ rails db:migrate`

Import all properties
`$ rake import_properties:all`
or just the original three
`$ rake import_properties:original`

Install node modules
`$ yarn`


### Starting

To launch app locally in development mode run
`$ foreman s`

Visit http://localhost:5100 in browser

### Tests

To run tests
`$ rspec spec`
