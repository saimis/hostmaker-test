namespace :import_properties do
  desc 'Imports properties for test'



    OWNDOMS_LIST = [
      {
        "owner": "carlos",
        "address": {
            "line1": "Flat 5",
            "line4": "7 Westbourne Terrace",
            "postCode": "W2 3UL",
            "city": "London",
            "country": "U.K."
        },
        "incomeGenerated": 2000.34
      },
      {
        "owner": "ankur",
        "address": {
            "line1": "4",
            "line2": "Tower Mansions",
            "line4": "86-87 Grange Road",
            "postCode": "SE1 3BW",
            "city": "London",
            "country": "U.K."
        },
        "incomeGenerated": 1000
      },
      {
        "owner": "elaine",
        "address": {
            "line1": "4",
            "line2": "332b",
            "line4": "Goswell Road",
            "postCode": "EC1V 7LQ",
            "city": "London",
            "country": "U.K."
        },
        "incomeGenerated": 1200
      },
      {
        "owner": "irene",
        "address": {
            "line1": "50",
            "line2": "Woodford",
            "line4": "Ingatestone Rd",
            "postCode": "IG8 9AL",
            "city": "London",
            "country": "U.K."
        },
        "incomeGenerated": 1140
      },

      {
        "owner": "lin",
        "address": {
            "line1": "2",
            "line2": "",
            "line4": "Maryland Road",
            "postCode": "E15 1JJ",
            "city": "London",
            "country": "U.K."
        },
        "incomeGenerated": 857
      },

      {
        "owner": "max",
        "address": {
            "line1": "126",
            "line4": " Girton Ave",
            "postCode": "NW9 9UD",
            "city": "London",
            "country": "U.K."
        },
        "incomeGenerated": 1210
      },
      {
        "owner": "chris",
        "address": {
            "line1": "6",
            "line2": "Fulham",
            "line4": "Sherbrooke Rd",
            "postCode": "SW6 7HU",
            "city": "London",
            "country": "U.K."
        },
        "incomeGenerated": 6012
      },
      {
        "owner": "megan",
        "address": {
            "line1": "17A",
            "line4": "Washington Ave",
            "postCode": "E12 5JA",
            "city": "London",
            "country": "U.K."
        },
        "incomeGenerated": 6012
      },
      {
        "owner": "chris",
        "address": {
            "line1": "9",
            "line4": "Baseing Cl",
            "postCode": "E6 5PJ",
            "city": "London",
            "country": "U.K."
        },
        "incomeGenerated": 150
      },
      {
        "owner": "chris",
        "address": {
            "line1": "68",
            "line4": "Herne Hill",
            "postCode": "SE24 9QP",
            "city": "London",
            "country": "U.K."
        },
        "incomeGenerated": 150
      },
    ]

  task all: :environment do
    impor_properties
  end

  task original: :environment do
    impor_properties(original: true)
  end

  def impor_properties(original=false)
    Owner.delete_all   if ActiveRecord::Base.connection.table_exists?('owners')
    Owndom.delete_all  if ActiveRecord::Base.connection.table_exists?('owndoms')
    City.delete_all    if ActiveRecord::Base.connection.table_exists?('cities')
    Country.delete_all if ActiveRecord::Base.connection.table_exists?('countries')

    list = original ? OWNDOMS_LIST.first(3) : OWNDOMS_LIST

    list.each do |owndom|
      city = City.find_or_create_by(name: owndom[:address][:city])
      country = Country.find_or_create_by(name: owndom[:address][:country])
      owner = Owner.find_or_create_by(name: owndom[:owner])

      Owndom.create(
        owner_id: owner.id,
        country_id: country.id,
        address1: owndom[:address][:line1] || '',
        address2: owndom[:address][:line2] || '',
        address3: owndom[:address][:line3] || '',
        address4: owndom[:address][:line4] || '',
        post_code: owndom[:address][:postCode] || '',
        city_id: city.id,
        income: owndom[:incomeGenerated] || 0
      )
    end
    puts "Properties imported"
  end
end
