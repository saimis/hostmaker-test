require 'rails_helper'

RSpec.describe Api::V1::Owndoms, type: :controller do
  subject(:owndoms) { described_class.new(params) }

  describe 'list' do
    context 'with no params' do
      let(:params) { {} }

      it 'returns all properties with owner' do
        owners = owndoms.list.map(&:owner).map(&:name)
        expect(owners).to match_array ["ankur", "carlos", "elaine", "irene", "lin", "max"]
      end

      it 'returns all properties' do
        expect(owndoms.list.count).to eq(6)
      end
    end

    context 'with params' do
      let(:params) { {query: 'carlos'} }

      it 'returns property with searched value' do
        expect(owndoms.list.first.owner.name).to eq('carlos')
      end
      it 'returns single property with searched owner' do
        expect(owndoms.list.count).to eq(1)
      end
    end

    describe 'show' do
      context 'without property id' do
        let(:params) { {id: nil} }

        it 'raises exception when record not found' do
          expect {owndoms.show}.to raise_exception(ActiveRecord::RecordNotFound)
        end
      end

      context 'with property id' do
        let(:params) { {id: 1} }
        let(:owndom) { build :owndom }

        it 'returns searched property' do
          expect(owndoms.show).to eq(owndom)
        end
      end
    end
  end
end
