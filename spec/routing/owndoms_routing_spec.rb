require "rails_helper"

RSpec.describe OwndomsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/owndoms").to route_to("owndoms#index")
    end

    it "routes to #new" do
      expect(:get => "/owndoms/new").to route_to("owndoms#new")
    end

    it "routes to #show" do
      expect(:get => "/owndoms/1").to route_to("owndoms#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/owndoms/1/edit").to route_to("owndoms#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/owndoms").to route_to("owndoms#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/owndoms/1").to route_to("owndoms#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/owndoms/1").to route_to("owndoms#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/owndoms/1").to route_to("owndoms#destroy", :id => "1")
    end

  end
end
