FactoryGirl.define do
  factory :owndom do
    id 1
    owner_id 1
    country_id 1
    city_id 1
    address1 'Flat 5'
    address2 ''
    address3 ''
    address4 '7 Westbourne Terrace'
    post_code ''
    income 0.200034e4
    created_at Time.zone.parse('2017-07-27 00:00:00')
    updated_at Time.zone.parse('2017-07-27 00:00:00')
  end
end
