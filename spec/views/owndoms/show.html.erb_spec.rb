require 'rails_helper'

RSpec.describe "owndoms/show", type: :view do
  before { skip }
  before(:each) do
    @owndom = assign(:owndom, Owndom.create!(
      :address1 => "Address1",
      :address2 => "Address2",
      :address3 => "Address3",
      :address4 => "Address4",
      :post_code => "Post Code",
      :income => "9.99"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Address1/)
    expect(rendered).to match(/Address2/)
    expect(rendered).to match(/Address3/)
    expect(rendered).to match(/Address4/)
    expect(rendered).to match(/Post Code/)
    expect(rendered).to match(/9.99/)
  end
end
