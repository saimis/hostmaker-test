require 'rails_helper'

RSpec.describe "owndoms/index", type: :view do
  before { skip }
  before(:each) do
    assign(:owndoms, [
      Owndom.create!(
        :address1 => "Address1",
        :address2 => "Address2",
        :address3 => "Address3",
        :address4 => "Address4",
        :post_code => "Post Code",
        :income => "9.99"
      ),
      Owndom.create!(
        :address1 => "Address1",
        :address2 => "Address2",
        :address3 => "Address3",
        :address4 => "Address4",
        :post_code => "Post Code",
        :income => "9.99"
      )
    ])
  end

  it "renders a list of owndoms" do
    render
    assert_select "tr>td", :text => "Address1".to_s, :count => 2
    assert_select "tr>td", :text => "Address2".to_s, :count => 2
    assert_select "tr>td", :text => "Address3".to_s, :count => 2
    assert_select "tr>td", :text => "Address4".to_s, :count => 2
    assert_select "tr>td", :text => "Post Code".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
  end
end
