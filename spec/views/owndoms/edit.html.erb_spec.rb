require 'rails_helper'

RSpec.describe "owndoms/edit", type: :view do
  before { skip }
  before(:each) do
    @owndom = assign(:owndom, Owndom.create!(
      :address1 => "MyString",
      :address2 => "MyString",
      :address3 => "MyString",
      :address4 => "MyString",
      :post_code => "MyString",
      :income => "9.99"
    ))
  end

  it "renders the edit owndom form" do
    render

    assert_select "form[action=?][method=?]", owndom_path(@owndom), "post" do

      assert_select "input[name=?]", "owndom[address1]"

      assert_select "input[name=?]", "owndom[address2]"

      assert_select "input[name=?]", "owndom[address3]"

      assert_select "input[name=?]", "owndom[address4]"

      assert_select "input[name=?]", "owndom[post_code]"

      assert_select "input[name=?]", "owndom[income]"
    end
  end
end
