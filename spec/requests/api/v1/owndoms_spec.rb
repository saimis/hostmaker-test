require 'rails_helper'

describe '/api/v1/owndoms', type: :request do
 let(:renderer) { ApplicationController.new }
 describe 'index' do
    context 'without format' do
      it 'renders not allowed status' do
        get '/api/v1/owndoms'
        expect(@response).to have_http_status(406)
        expect(@response.body).to eql('{"error":"Format not allowed"}')
      end
    end

    context 'with format' do
      let(:existing_id) { 1 }
      let(:nonexisting_id) { 1000 }

      it 'renders all records with template' do
        get '/api/v1/owndoms.json'

        expected_val = renderer.render_to_string('api/v1/owndoms/index', locals: {:@owndoms => Owndom.all})

        expect(@response.body).to eql(expected_val)
      end

      it 'renders single record with partial' do
        get "/api/v1/owndoms/#{existing_id}.json"

        expected_val = renderer.render_to_string('api/v1/owndoms/_owndom', locals: {owndom: Owndom.first})

        expect(@response.body).to eql(expected_val)
      end

      it 'renders error when record not found' do
        get "/api/v1/owndoms/#{nonexisting_id}.json"

        expect(@response).to have_http_status(404)
        expect(@response.body).to eql("{\"error\":\"Couldn't find Owndom with 'id'=#{nonexisting_id}\"}")
      end
    end
  end
end
